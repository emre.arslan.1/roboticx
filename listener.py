# !/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from time import sleep
import RPi.GPIO as GPIO
class Motor():
	def __init__(self, p1, p2, v, correctie = 1):
		self.p1 = p1
		self.p2 = p2
		self.velocity = v
		GPIO.setup(p1, GPIO.OUT)
		GPIO.setup(p2, GPIO.OUT)
		self.pA = GPIO.PWM(p1,20)
		self.pB = GPIO.PWM(p2,20)
		# correctie toe te passen op de berekeningen bij "linear turn" => enige relevante functie voor line_detection
		self.c = correctie
 

	# x != 0 z = 0
	def runlinear(self, data):
		# Negatieve snelheid
		if data.linear.x < 0:
			rospy.loginfo('achteruit')
			self.pA.ChangeDutyCycle(self.velocity)
			GPIO.output(self.p1, True)
			self.pB.ChangeDutyCycle(self.velocity)
			GPIO.output(self.p2, False)
		# positieve snelheid
		elif data.linear.x > 0:
			self.pA.ChangeDutyCycle(self.velocity)
			GPIO.output(self.p1, False)
			self.pB.ChangeDutyCycle(self.velocity)
			GPIO.output(self.p2, True)
		# de x is nul => traag afbouwen
		else:
			GPIO.output(self.p1, False)
			GPIO.output(self.p2, False)
	# x = 0 z !=0
	def turn(self, z):
		vel =  z * r * self.c
		if vel > 0: #motor wordt in vooruit gezet
			rospy.loginfo('vooruit')
			GPIO.output(self.p1, False)
			GPIO.output(self.p2, True)
		else: #motor wordt in achteruit gezet
			rospy.loginfo('achteruit')
			GPIO.output(self.p1, True)
			GPIO.output(self.p2, False)
			vel = -vel
		rospy.loginfo(vel)
		# vel moet wel positief zijn
		self.pA.ChangeDutyCycle(vel)
		self.pB.ChangeDutyCycle(vel)
	# x =! 0 z !=0
	def linearturn(self,x,z):
		if x < 0:
			GPIO.output(self.p1, True)
			GPIO.output(self.p2, False)
			vel = self.velocity - z * r * self.c
		else: #x > 0 motors omgekeerd (vooruit rijden)
                         GPIO.output(self.p1, False)
                         GPIO.output(self.p2, True)
                         vel = self.velocity - z * r * self.c
		rospy.loginfo(vel)
		self.pA.ChangeDutyCycle(vel)
		self.pB.ChangeDutyCycle(vel)
	def stop(self):
		GPIO.output(self.p1,False)
		GPIO.output(self.p2,False)
	


def callback(data):
    rospy.loginfo(data.linear.x)
    rospy.loginfo(data.angular.z)
    xval = data.linear.x
    zval = data.angular.z
    if xval != 0:
	    if zval == 0:
	    		motor_left.runlinear(data)
	    		motor_right.runlinear(data)
	    else:
	# Belangrijk voor line_detection Als de robot omgekeerd op de twist messages types reageert, doe dan -zval bij motor_left en zval bij motor_right
			motor_left.linearturn(xval,zval)
			motor_right.linearturn(xval,-zval)
    else:
	    if zval != 0: #Z!=0 && x=0

			motor_left.turn(-zval)
			motor_right.turn(zval)
	    else : #z & x == 0 => runlin met x = 0 stopt de robot
			motor_left.runlinear(data)
			motor_right.runlinear(data)

def listener():

    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', Twist, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    	    print 'confing'
	    r = 0.10
	    GPIO.setmode(GPIO.BCM)
	    motor_left = Motor(17,22,20)
	    motor_right = Motor(23,24,20)
	    listener()
	    GPIO.cleanup()
	    print('cleaned')

