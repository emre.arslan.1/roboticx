#!/usr/bin/env python

# All imported packages
import rospy
import cv2 as opencv
import numpy
from geometry_msgs.msg import Twist

# Select the input video capture device - 0 = Raspi Camera V2 (first detected device)
cap = opencv.VideoCapture(0)

# ROS config
rospy.init_node('lane', anonymous=False)
# ROS topic to publish to (Twist messages for the motor node)
publishTopic = rospy.Publisher('chatter', Twist, queue_size=1)
twist = Twist()

# Change the capture resolution - to increase speed and processing
def wijzig_Resolutie():
    cap.set(3, 480)
    cap.set(4, 320)

wijzig_Resolutie()

# Loop to keep on capturing until cap is closed or unable to function
while(True):
    # Read the data coming from the capture device into frame - if capture is ok, ret is true, else we stop looping
    ret, frame = cap.read()

    # Change the frame from BGR (opencv default) to HSV collor scheme
    hsv = opencv.cvtColor(frame, opencv.COLOR_BGR2HSV)
    # Ranges to detect any blue color
    # Range 1 - Personal
    lower_blue = numpy.array([20, 50, 50])
    upper_blue = numpy.array([40, 255, 255])


    # Masking using the blue limits
    mask = opencv.inRange(hsv, lower_blue, upper_blue)

    # Apply Canny edge - The second and third parameters are lower and upper ranges for edge detection
    edges = opencv.Canny(mask, 200, 400)

    # Define a region of interest on the frame
    # Get the height and width of the image to make a polygon (kind of triangle)
    height, width = edges.shape
    mask = numpy.zeros_like(edges)
    # Shape over the frame to only be able to see the bottom half of the screen
    polygon = numpy.array([[
        (0, height * 1 / 2),
        (width, height * 1 / 2),
        (width, height),
        (0, height),
    ]], numpy.int32)

    # Fill and merge the mask with the edges image to form a cropped image called cropped_edges
    opencv.fillPoly(mask, polygon, 255)
    cropped_edges = opencv.bitwise_and(edges, mask)

    # Using the HoughLinesP function (HoughLineP detects lines using Polar Coordinates = elevation angle and distance from the origin)
    # Distance precision in pixel
    rho = 1
    # Angular precision in radian
    angle = numpy.pi / 180
    # Minimal amount of votes/guesses
    min_threshold = 10
    # All combined - return line_segments
    line_segments = opencv.HoughLinesP(cropped_edges, rho, angle, min_threshold, numpy.array([]), minLineLength=10, maxLineGap=1)

    # Converting detected lines to array and then into Twist messages
    if line_segments is not None:
        vMean = [0,0,0,0]
        # Looping through all the detected lines (line_segments)
        for i in range(len(line_segments)):
            if line_segments[i][0][1] < line_segments[i][0][3]:
                vMean[0] += line_segments[i][0][0]
                vMean[1] += line_segments[i][0][1]
                vMean[2] += line_segments[i][0][2]
                vMean[3] += line_segments[i][0][3]
            elif line_segments[i][0][1] >= line_segments[i][0][3]:
                vMean[0] += line_segments[i][0][2]
                vMean[1] += line_segments[i][0][3]
                vMean[2] += line_segments[i][0][0]
                vMean[3] += line_segments[i][0][1]
        for i in range(len(vMean)):
            vMean[i] = vMean[i]/len(line_segments)

        # This mathematical function helps user to calculate inverse tangent for all x(being the array elements)
        angularZCalcul = numpy.arctan((vMean[3]-vMean[1])/(vMean[2]-vMean[0]))
        twist.linear.x = 0.3
        twist.angular.z = angularZCalcul
        publishTopic.publish(twist)

    # Actual display output - disable if not using
    opencv.imshow('frame',cropped_edges)
    if opencv.waitKey(1) & 0xFF == ord('q'):
        break

# Release the video capture device, best practice
cap.release()

# Uncomment if using display ouput
opencv.destroyAllWindows()
