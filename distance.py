#!/usr/bin/env python

# license removed for brevity
import rospy
import RPi.GPIO as GPIO
import time
from geometry_msgs.msg import Twist

pub = rospy.Publisher('chatter', Twist, queue_size=10)
rospy.init_node('talker', anonymous=True)
rate = rospy.Rate(10)  # 10hz
GPIO.setmode(GPIO.BOARD)

PIN_TRIGGER = 13
PIN_ECHO = 37

GPIO.setup(PIN_TRIGGER, GPIO.OUT)
GPIO.setup(PIN_ECHO, GPIO.IN)

GPIO.output(PIN_TRIGGER, GPIO.LOW)

print "Waiting for sensor to settle"

time.sleep(2)
while True: 
        time.sleep(0.00001)

	GPIO.output(PIN_TRIGGER, GPIO.HIGH)

	time.sleep(0.00001)

	GPIO.output(PIN_TRIGGER, GPIO.LOW)

	while GPIO.input(PIN_ECHO) == 0:
            pulse_start_time = time.time()
	while GPIO.input(PIN_ECHO) == 1:
            pulse_end_time = time.time()
        time.sleep(0.1)

	pulse_duration = pulse_end_time - pulse_start_time
	distance = round(pulse_duration * 17150, 2)
	print "Distance:", distance, "cm"
	if (distance <= 25):
		print "STOPPEN"

		msg = Twist()
		msg.linear.x = 0
		msg.angular.x = 0
		pub.publish(msg)
	else :
		print "NIET STOPPEN"

