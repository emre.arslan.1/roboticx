#!/usr/bin/env python

from __future__ import print_function

#import roslib; roslib.load_manifest('keyboard')
import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import String

import sys, select, termios, tty

msg = """
Reading from the keyboard  and Publishing to Twist!
---------------------------
Moving around:
        z
   q    s    d
Change speed:
-  w    +    c
anything else : stop
CTRL-C to quit
"""


twist = Twist()

pub = rospy.Publisher('chatter', Twist, queue_size = 1)

moveBindings = {
        'z':(1,0),
        's':(-1,0),
        'q':(0,1),
        'd':(0,-1)
    }
def getKey():
    tty.setraw(sys.stdin.fileno())
    select.select([sys.stdin], [], [], 0)
    key = sys.stdin.read(1)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

def vels(speed,turn):
    return "currently:\tspeed %s\tturn %s " % (speed,turn)

def callback(response):
    rospy.loginfo(response)
    if response.data == "stop":
	twist.linear.x = 0
	twist.angular.z = 0
	pub.publish(twist)


if __name__=="__main__":
    settings = termios.tcgetattr(sys.stdin)

    rospy.init_node('keyboard')

    speed = rospy.get_param("~speed", 0.5)
    turn = rospy.get_param("~turn", 1.0)
    x = 0
    y = 0
    z = 0
    th = 0
    status = 0

    rospy.Subscriber("sensor", String, callback)

    try:
        print(msg)
        print(vels(speed,turn))

        while(1):
	    global isGo
            key = ' '
            key = getKey()
            if key in moveBindings.keys():
                x = moveBindings[key][0]
                th = moveBindings[key][1]
	    elif key == 'c':
		speed = speed + 0.1
		if speed > 1:
		    speed = 1.0
	    elif key == 'w':
		speed = speed - 0.1
		if speed < 0:
		    speed = 0.0
            else:
                x = 0
                y = 0
                z = 0
                th = 0
                if (key == '\x03'):
                    break

            if isGo:
		twist.linear.x = x * speed
            	twist.angular.z = th * speed
	    else:
		twist.linear.x = 0
		twist.angular.z = 0

            pub.publish(twist)

    except Exception as e:
        print(e)

    finally:
        twist.linear.x = 0; twist.linear.y = 0; twist.linear.z = 0
        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
        pub.publish(twist)

        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
